package com.fadhifatah.wifimonitoring.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

public class WifiConnectivityReceiver extends BroadcastReceiver {

    private WifiConnectivityListener wifiConnectivityListener;

    public interface WifiConnectivityListener {
        void onWifiConnectionChange(boolean isConnect, String ssid);
    }

    public WifiConnectivityReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("WIFI_MONITORING", "onReceive");
        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);

        if (networkInfo != null) {
            WifiManager wifiManager =
                    (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

            WifiInfo wifiInfo = wifiManager.getConnectionInfo();

            wifiConnectivityListener.onWifiConnectionChange(networkInfo.isConnected() && (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) && wifiManager.isWifiEnabled(), wifiInfo.getSSID());
        }
    }

    public void setWifiConnectivityListener(WifiConnectivityListener wifiConnectivityListener) {
        this.wifiConnectivityListener = wifiConnectivityListener;
    }
}
