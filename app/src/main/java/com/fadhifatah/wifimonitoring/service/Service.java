package com.fadhifatah.wifimonitoring.service;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface Service {

    @GET
    Call<String> get(@Url String url);

    @GET("{endpoint}")
    Call<String> get1(@Path("endpoint") String endpoint);

    @GET("{endpoint}")
    Call<String> get2(@Path("endpoint") String endpoint, @QueryMap Map<String, String> options);

}
