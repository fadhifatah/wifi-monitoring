package com.fadhifatah.wifimonitoring.ui.main;

import android.Manifest;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.*;
import com.fadhifatah.wifimonitoring.R;
import com.fadhifatah.wifimonitoring.service.Service;
import com.fadhifatah.wifimonitoring.util.WifiConnectivityReceiver;
import okhttp3.*;
import okhttp3.internal.http.HttpHeaders;
import okio.Buffer;
import okio.BufferedSource;
import okio.GzipSource;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import java.io.EOFException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements WifiConnectivityReceiver.WifiConnectivityListener {

    private static final Charset UTF8 = Charset.forName("UTF-8");

    Button mBWiFiPermission;
    Button mBSaveUrl;
    Button mBSendRequest;
    AutoCompleteTextView mETRequestURL;
    ProgressBar mPBLoading;
    TextView mTVWifiSSID;
    TextView mTVResponse;

    IntentFilter mIntentFilter;
    WifiConnectivityReceiver mWifiConnectivityReceiver;
    Service mService;
    boolean flagConnected = false;
    private SharedPreferences urlPrefs;
    private ArrayAdapter<String> adapter;
    private List<String> savedUrls;

    OkHttpClient mOkHttpClient =
            new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @NonNull
                        @Override
                        public Response intercept(@NonNull Chain chain) throws IOException {
                            final StringBuilder stringBuilder = new StringBuilder();
                            Request request = chain.request();

                            RequestBody requestBody = request.body();
                            boolean hasRequestBody = requestBody != null;

                            Connection connection = chain.connection();
                            String requestStartMessage = "--> "
                                    + request.method()
                                    + ' ' + request.url()
                                    + (connection != null ? " " + connection.protocol() : "");
                            if (hasRequestBody) {
                                requestStartMessage += " (" + requestBody.contentLength() + "-byte body)";
                            }
                            stringBuilder.append(requestStartMessage).append("\n");

                            if (hasRequestBody) {
                                // Request body headers are only present when installed as a network interceptor. Force
                                // them to be included (when available) so there values are known.
                                if (requestBody.contentType() != null) {
                                    stringBuilder.append("Content-Type: ").append(requestBody.contentType()).append("\n");
                                }
                                if (requestBody.contentLength() != -1) {
                                    stringBuilder.append("Content-Length: ").append(requestBody.contentLength()).append("\n");
                                }
                            }

                            Headers headers = request.headers();
                            for (int i = 0, count = headers.size(); i < count; i++) {
                                String name = headers.name(i);
                                // Skip headers from the request body as they are explicitly logged above.
                                if (!"Content-Type".equalsIgnoreCase(name) && !"Content-Length".equalsIgnoreCase(name)) {
                                    stringBuilder.append(name).append(": ").append(headers.value(i)).append("\n");
                                }
                            }

                            if (!hasRequestBody) {
                                stringBuilder.append("--> END ").append(request.method()).append("\n");
                            } else if (bodyHasUnknownEncoding(request.headers())) {
                                stringBuilder.append("--> END ").append(request.method()).append(" (encoded body omitted)").append("\n");
                            } else {
                                Buffer buffer = new Buffer();
                                requestBody.writeTo(buffer);

                                Charset charset = UTF8;
                                MediaType contentType = requestBody.contentType();
                                if (contentType != null) {
                                    charset = contentType.charset(UTF8);
                                }

                                stringBuilder.append("\n");
                                if (isPlaintext(buffer)) {
                                    if (charset != null) {
                                        stringBuilder.append(buffer.readString(charset)).append("\n");
                                    }
                                    stringBuilder.append("--> END ").append(request.method()).append(" (").append(requestBody.contentLength()).append("-byte body)").append("\n");
                                } else {
                                    stringBuilder.append("--> END ").append(request.method()).append(" (binary ").append(requestBody.contentLength()).append("-byte body omitted)").append("\n");
                                }
                            }

                            long startNs = System.nanoTime();
                            Response response;
                            try {
                                response = chain.proceed(request);
                            } catch (Exception e) {
                                stringBuilder.append("<-- HTTP FAILED: ").append(e).append("\n");
                                throw e;
                            }
                            long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);

                            ResponseBody responseBody = response.body();
                            long contentLength = responseBody.contentLength();
                            String bodySize = contentLength != -1 ? contentLength + "-byte" : "unknown-length";
                            stringBuilder.append("<-- ").append(response.code()).append(response.message().isEmpty() ? "" : ' ' + response.message()).append(' ').append(response.request().url()).append(" (").append(tookMs).append("ms").append(", ").append(bodySize).append(" body").append(')').append("\n");

                            Headers headers2 = response.headers();
                            for (int i = 0, count = headers2.size(); i < count; i++) {
                                stringBuilder.append(headers2.name(i)).append(": ").append(headers2.value(i)).append("\n");
                            }

                            if (!HttpHeaders.hasBody(response)) {
                                stringBuilder.append("<-- END HTTP").append("\n");
                            } else if (bodyHasUnknownEncoding(response.headers())) {
                                stringBuilder.append("<-- END HTTP (encoded body omitted)").append("\n");
                            } else {
                                BufferedSource source = responseBody.source();
                                source.request(Long.MAX_VALUE); // Buffer the entire body.
                                Buffer buffer = source.buffer();

                                Long gzippedLength = null;
                                if ("gzip".equalsIgnoreCase(headers2.get("Content-Encoding"))) {
                                    gzippedLength = buffer.size();
                                    GzipSource gzippedResponseBody = null;
                                    try {
                                        gzippedResponseBody = new GzipSource(buffer.clone());
                                        buffer = new Buffer();
                                        buffer.writeAll(gzippedResponseBody);
                                    } finally {
                                        if (gzippedResponseBody != null) {
                                            gzippedResponseBody.close();
                                        }
                                    }
                                }

                                Charset charset = UTF8;
                                MediaType contentType = responseBody.contentType();
                                if (contentType != null) {
                                    charset = contentType.charset(UTF8);
                                }

                                if (!isPlaintext(buffer)) {
                                    stringBuilder.append("\n");
                                    stringBuilder.append("<-- END HTTP (binary ").append(buffer.size()).append("-byte body omitted)").append("\n");
                                    return response;
                                }

                                if (contentLength != 0) {
                                    stringBuilder.append("\n");
                                    stringBuilder.append(buffer.clone().readString(charset)).append("\n");
                                }

                                if (gzippedLength != null) {
                                    stringBuilder.append("<-- END HTTP (").append(buffer.size()).append("-byte, ").append(gzippedLength).append("-gzipped-byte body)").append("\n");
                                } else {
                                    stringBuilder.append("<-- END HTTP (").append(buffer.size()).append("-byte body)").append("\n");
                                }
                            }

                            if (mTVResponse != null) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mTVResponse.setText(stringBuilder.toString());
                                    }
                                });
                            }
                            return response;
                        }
                    })
                    .followRedirects(false)
                    .build();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBWiFiPermission = findViewById(R.id.main_btn_permission);
        mBSaveUrl = findViewById(R.id.main_btn_save);
        mBSendRequest = findViewById(R.id.main_send_request);
        mETRequestURL = findViewById(R.id.main_request_url);
        mPBLoading = findViewById(R.id.main_progress);
        mTVWifiSSID = findViewById(R.id.main_wifi_ssid);
        mTVResponse = findViewById(R.id.main_response);

        mWifiConnectivityReceiver = new WifiConnectivityReceiver();
        mWifiConnectivityReceiver.setWifiConnectivityListener(this);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        mIntentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);

        urlPrefs = getSharedPreferences("urlPrefs", Context.MODE_PRIVATE);

        savedUrls = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.array_url_get_example)));
        savedUrls.addAll(Arrays.asList(urlPrefs.getString("urls", "").split(",")));

        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, savedUrls);
        mETRequestURL.setAdapter(adapter);


        mETRequestURL.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().length()>0 && isNewUrl(editable.toString()))
                    mBSaveUrl.setEnabled(true);
                else
                    mBSaveUrl.setEnabled(false);
            }
        });

        mBWiFiPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestWiFiStatePermission();
            }
        });

        togglePermissionButton();

        mBSaveUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNewUrl(mETRequestURL.getText().toString()))
                    saveUrl(mETRequestURL.getText().toString());
            }
        });

        mBSendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = mETRequestURL.getText().toString();
                if (!TextUtils.isEmpty(url) && Patterns.WEB_URL.matcher(url).matches()) {
                    setActionView(false);
                    mPBLoading.setVisibility(View.VISIBLE);
                    mTVResponse.setText("");
                    try {
                        Log.d("WIFI_MONITORING", "url: " + url);
                        Retrofit retrofit =
                                new Retrofit.Builder()
                                        .baseUrl(getBaseUrl(url))
                                        .client(mOkHttpClient)
                                        .addConverterFactory(ScalarsConverterFactory.create())
                                        .build();

                        mService = retrofit.create(Service.class);

//                        mService.get(url).enqueue(new retrofit2.Callback<String>() {
                        retrofit2.Call<String> call = getEndPoint(url) == null ? mService.get(url) : getParams(url) == null ? mService.get1(getEndPoint(url)) : mService.get2(getEndPoint(url), getParams(url));
                        call.enqueue(new retrofit2.Callback<String>() {
                            @Override
                            public void onResponse(@NonNull retrofit2.Call<String> call, @NonNull retrofit2.Response<String> response) {
                                if (response.isSuccessful()) {
                                    Toast.makeText(getApplicationContext(), "Request successful", Toast.LENGTH_SHORT).show();
                                }

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (flagConnected) {
                                            setActionView(true);
                                        }
                                        mPBLoading.setVisibility(View.GONE);
                                    }
                                });
                            }

                            @Override
                            public void onFailure(@NonNull retrofit2.Call<String> call, @NonNull Throwable t) {
                                Toast.makeText(getApplicationContext(), "Request unsuccessful", Toast.LENGTH_SHORT).show();

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (flagConnected) {
                                            setActionView(true);
                                        }
                                        mPBLoading.setVisibility(View.GONE);
                                    }
                                });
                            }
                        });
                    } catch (IllegalArgumentException e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                        setActionView(true);
                        mPBLoading.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter request URL, correctly", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mTVResponse.setTypeface(Typeface.MONOSPACE);
        mETRequestURL.setEnabled(false);
        mBSendRequest.setEnabled(false);

        setActionView(false);
    }

    private void setActionView(boolean isActivate) {
        mETRequestURL.setEnabled(isActivate);
        mBSendRequest.setEnabled(isActivate);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mWifiConnectivityReceiver, mIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mWifiConnectivityReceiver);

        if (mService != null) {
            mService = null;
        }

        mPBLoading.setVisibility(View.GONE);
    }

    @Override
    public void onWifiConnectionChange(boolean isConnect, String ssid) {
        flagConnected = isConnect;
        Log.d("WIFI_MONITORING", "onWifiConnectionChange: " + isConnect + " " + ssid);
        togglePermissionButton();
        if (isConnect) {
            Toast.makeText(getApplicationContext(), "Connected to Wifi", Toast.LENGTH_SHORT).show();
            mTVWifiSSID.setText(getString(R.string.main_text_wifi_ssid, ssid));
            mTVWifiSSID.setTextColor(getResources().getColor(R.color.colorPrimary));

            setActionView(true);
        } else {
            Toast.makeText(getApplicationContext(), "Disconnected from Wifi", Toast.LENGTH_SHORT).show();
            mTVWifiSSID.setText(getString(R.string.main_text_wifi_off));
            mTVWifiSSID.setTextColor(getResources().getColor(R.color.colorAccent));
            mPBLoading.setVisibility(View.GONE);

            if (mService != null) {
                mService = null;
            }

            setActionView(false);
        }
    }

    private boolean bodyHasUnknownEncoding(Headers headers) {
        String contentEncoding = headers.get("Content-Encoding");
        return contentEncoding != null
                && !contentEncoding.equalsIgnoreCase("identity")
                && !contentEncoding.equalsIgnoreCase("gzip");
    }

    /**
     * Returns true if the body in question probably contains human readable text. Uses a small sample
     * of code points to detect unicode control characters commonly used in binary file signatures.
     */
    private static boolean isPlaintext(Buffer buffer) {
        try {
            Buffer prefix = new Buffer();
            long byteCount = buffer.size() < 64 ? buffer.size() : 64;
            buffer.copyTo(prefix, 0, byteCount);
            for (int i = 0; i < 16; i++) {
                if (prefix.exhausted()) {
                    break;
                }
                int codePoint = prefix.readUtf8CodePoint();
                if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException e) {
            return false; // Truncated UTF-8 sequence.
        }
    }

    private boolean isNewUrl(String check){
        for(String s : savedUrls)
            if(check.equalsIgnoreCase(s))
                return false;
        return true;
    }

    private void saveUrl(String save){
        urlPrefs.edit().putString("urls",urlPrefs.getString("urls","")+save+",").apply();
        adapter.add(save);
        mBSaveUrl.setEnabled(false);
        Toast.makeText(this,"Url Saved",Toast.LENGTH_SHORT).show();
    }

    private void togglePermissionButton(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O_MR1 &&
                checkAccessWiFiStatePermissionDisabled())
            mBWiFiPermission.setVisibility(View.VISIBLE);
        else
            mBWiFiPermission.setVisibility(View.GONE);
    }

    private boolean checkAccessWiFiStatePermissionDisabled(){
        return ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED;
    }

    String[] wifiPermission = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION};

    private void requestWiFiStatePermission(){
        if(checkAccessWiFiStatePermissionDisabled())
            ActivityCompat.requestPermissions(this, wifiPermission,100);
        else {
            Toast.makeText(getApplicationContext(), "Permission Already Granted", Toast.LENGTH_SHORT).show();
            togglePermissionButton();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case 100:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    togglePermissionButton();
                    Toast.makeText(this,"Permission Granted",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private String getBaseUrl(String url) {
        int endPointIndex = url.lastIndexOf("/");
        if(endPointIndex > 0) {
            Log.d("WIFI_MONITORING", "Baseurl: "+url.substring(0, endPointIndex));
            return url.substring(0, endPointIndex+1);
        }
        else return url;
    }

    private String getEndPoint(String url) {
        int paramIndex = url.indexOf("?");
        String urlNoParams = url;
        if (paramIndex > 0) {
            urlNoParams = url.substring(0,paramIndex);
        }


        int endPointIndex = urlNoParams.lastIndexOf("/");

        if(endPointIndex > 0 && urlNoParams.length()>endPointIndex) {
            if (paramIndex > 0) {
                Log.d("WIFI_MONITORING", "Endpoint: "+urlNoParams.substring(endPointIndex, paramIndex));
                return urlNoParams.substring(endPointIndex+1, paramIndex);
            }else {
                Log.d("WIFI_MONITORING", "Endpoint: "+urlNoParams.substring(endPointIndex, urlNoParams.length()));
                return urlNoParams.substring(endPointIndex+1, urlNoParams.length());
            }
        }else return null;
    }

    private Map<String, String> getParams(String url) {
        int paramIndex = url.indexOf("?");
        if(paramIndex>0){
            String params = url.substring(paramIndex+1, url.length());
            Map<String, String> queries = new HashMap<>();
            for(String entry: params.split("&")){
                String[] keyValue = entry.split("=");
                if(keyValue.length>1) queries.put(keyValue[0],keyValue[1]);
                else queries.put(keyValue[0],"");

            }
            Log.d("WIFI_MONITORING", "Params: "+queries.toString());
            return queries;
        }
        else return null;
    }

    private String getStringParams(String url){
        int paramIndex = url.indexOf("?");
        if(paramIndex>0){
            return url.substring(paramIndex, url.length());
        }
        else return null;
    }
}
